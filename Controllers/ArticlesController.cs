using Newtonsoft.Json;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace dotnet_core_web_api_with_ng.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("CorsPolicy")]
    public class ArticlesController : Controller
    {
        private string HackerNewsUrl = "https://hacker-news.firebaseio.com/v0/";
        private static HttpClient client;
        
        // Use float in the method signature to allow Infinity 
        [HttpGet("[action]/{count}")]
        public async Task<IActionResult> GetBest(float count)
        {
            try
            {
                if (client == null) {
                    client = getConnection();
                }
                var response = await client.GetAsync($"beststories.json?print=pretty");
                response.EnsureSuccessStatusCode();

                var stringResult = await response.Content.ReadAsStringAsync();
                var ids = JsonConvert.DeserializeObject<int[]>(stringResult);

                if (ids.Length > count) {
                    Array.Resize(ref ids, (int)count);
                }
    
                return Ok(new {
                    ids
                    });
            }
            catch (HttpRequestException e)
            {
                return BadRequest($"Error getting Hacker News Articles: {e.Message}");
            }
        }

        // Use float in the method signature to allow Infinity 
        [HttpGet("[action]/{count}")]
        public async Task<IActionResult> GetNew(float count)
        {
            try
            {
                if (client == null) {
                    client = getConnection();
                }
                var response = await client.GetAsync($"newstories.json?print=pretty");
                response.EnsureSuccessStatusCode();

                var stringResult = await response.Content.ReadAsStringAsync();
                var ids = JsonConvert.DeserializeObject<int[]>(stringResult);

                if (ids.Length > count) {
                    Array.Resize(ref ids, (int)count);
                }
    
                return Ok(new {
                    ids
                    });
            }
            catch (HttpRequestException e)
            {
                return BadRequest($"Error getting Hacker News Articles: {e.Message}");
            }
        }

        // Use float in the method signature to allow Infinity 
        [HttpGet("[action]/{count}")]
        public async Task<IActionResult> GetTop(float count)
        {
            try
            {
                if (client == null) {
                    client = getConnection();
                }
                var response = await client.GetAsync($"topstories.json?print=pretty");
                response.EnsureSuccessStatusCode();

                var stringResult = await response.Content.ReadAsStringAsync();
                var ids = JsonConvert.DeserializeObject<int[]>(stringResult);

                if (ids.Length > count) {
                    Array.Resize(ref ids, (int)count);
                }
    
                return Ok(new {
                    ids
                    });
            }
            catch (HttpRequestException e)
            {
                return BadRequest($"Error getting Hacker News Articles: {e.Message}");
            }
        }


        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<Article>> GetArticleById (int id) {
            try
            {
                if (client == null) {
                    client = getConnection();
                }
                var response = await client.GetAsync($"item/{id}.json");
                response.EnsureSuccessStatusCode();

                var stringResult = await response.Content.ReadAsStringAsync();
                Article rawArticle = JsonConvert.DeserializeObject<Article>(stringResult);
                return Ok(new {
                    rawArticle
                });
            }
            catch (HttpRequestException e)
            {
                return BadRequest($"Error getting Hacker News Article with id - {id}: {e.Message}");
            }
        }

        private HttpClient getConnection() {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.ConnectionClose = true;
            client.BaseAddress = new Uri(this.HackerNewsUrl);
            return client;
        }
    }
}
