// Sample article from API
// {
//     "by": "spking",
//     "descendants": 0,
//     "id": 19928776,
//     "score": 2,
//     "time": 1558013131,
//     "title": "Doctor gives hundreds of people HIV with contaminated needle",
//     "type": "story",
//     "url": "https://www.independent.co.uk/news/world/asia/pakistan-hiv-aids-doctor-larkana-rato-dero-sindh-a8916591.html"
// }

public class Article
{
  public string by { get; set; }

  public int descendants { get; set; }
  
  public int id { get; set; }

  public int score { get; set; }

  public int time { get; set; }

  public string title { get; set; }
  
  public string type { get; set; }
  
  public string url { get; set; }
}