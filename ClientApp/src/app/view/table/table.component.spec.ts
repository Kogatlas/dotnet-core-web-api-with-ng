import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {
  MatFormFieldModule,
  MatInputModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatSelectModule,
  MatSliderModule,
  MatTableDataSource,
  MatTableModule,
} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Store, StoreModule } from '@ngrx/store';
import * as _ from 'lodash';
import { of } from 'rxjs';
import * as fromStore from '../../reducers';
import { metaReducers, reducers } from '../../reducers';

import { ArticlesService } from '../shared/articles.service';
import { TableComponent } from './table.component';
import { AddArticle } from 'src/app/entities/article/article.actions';

describe('TableComponent', () => {
  let articlesService: {
    getArticleById: jasmine.Spy,
    getArticleIdsByCount: jasmine.Spy
  };
  let component: TableComponent;
  let fixture: ComponentFixture<TableComponent>;
  let store: Store<fromStore.State>;

  const testArticles = [
    { 'id': 0, 'by': 'testAuthor0', 'title': 'testTitle0', 'url': 'testUrl0', sources: ['best'] },
    { 'id': 1, 'by': 'testAuthor1', 'title': 'testTitle1', 'url': 'testUrl1', sources: ['best'] },
    { 'id': 2, 'by': 'testAuthor2', 'title': 'testTitle2', 'url': 'testUrl2', sources: ['best'] },
    { 'id': 3, 'by': 'testAuthor3', 'title': 'testTitle3', 'url': 'testUrl3', sources: ['best'] },
    { 'id': 4, 'by': 'testAuthor4', 'title': 'testTitle4', 'url': 'testUrl4', sources: ['best'] },
    { 'id': 5, 'by': 'testAuthor5', 'title': 'testTitle5', 'url': 'testUrl5', sources: ['best'] },
    { 'id': 6, 'by': 'testAuthor6', 'title': 'testTitle6', 'url': 'testUrl6', sources: ['best'] },
    { 'id': 7, 'by': 'testAuthor7', 'title': 'testTitle7', 'url': 'testUrl7', sources: ['best'] },
    { 'id': 8, 'by': 'testAuthor8', 'title': 'testTitle8', 'url': 'testUrl8', sources: ['best'] },
    { 'id': 9, 'by': 'testAuthor9', 'title': 'testTitle9', 'url': 'testUrl9', sources: ['best'] }
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableComponent ],
      imports: [
        BrowserAnimationsModule,
        HttpClientTestingModule,
        MatFormFieldModule,
        MatInputModule,
        MatPaginatorModule,
        MatProgressBarModule,
        MatProgressSpinnerModule,
        MatSelectModule,
        MatSliderModule,
        MatTableModule,
        StoreModule.forRoot(reducers, { metaReducers })
      ],
      providers: [
        { provide: ArticlesService, useValue: jasmine.createSpyObj('ArticlesService', [ 'getArticleIdsByCount', 'getArticleById' ]) }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableComponent);
    component = fixture.componentInstance;
    store = TestBed.get(Store);
    testArticles.forEach(article => {
      store.dispatch(new AddArticle({ article }));
    });
    articlesService = TestBed.get(ArticlesService);

    // TODO: Clean this logic up or comment well
    (articlesService.getArticleById as jasmine.Spy).and.callFake((id: number) => {
      return of({rawArticle: testArticles.filter(article => article.id === id)[0]});
    });
    // We intentionally return an invalid id (0) here to test resiliency
    const ids = _.range(10);
    (articlesService.getArticleIdsByCount as jasmine.Spy).and.returnValue(of({ids}));

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not invoke the paginator method "firstPage()" upon filtering if pagination is not available', () => {
    component.dataSource.paginator = null;
    spyOn(component.paginator, 'firstPage');
    component.filter('test');
    fixture.detectChanges();

    expect(component.paginator.firstPage).toHaveBeenCalledTimes(0);
  });

  it('should invoke the paginator method "firstPage()" upon filtering if pagination is available', () => {
    component.dataSource.paginator = component.paginator;
    spyOn(component.paginator, 'firstPage');
    component.filter('test');
    fixture.detectChanges();

    expect(component.paginator.firstPage).toHaveBeenCalled();
  });

  it('should call openArticle() with the correct url value when a title is clicked ', () => {
    spyOn(component, 'openArticle');
    component.dataSource = new MatTableDataSource(testArticles);
    fixture.detectChanges();
    // Find and click an element in the table
    const element = fixture.nativeElement.querySelector('.title');
    element.click();

    // Test against the expected url value
    expect(component.openArticle).toHaveBeenCalledWith(testArticles.find(article => article.title === element.innerText).url);
  });

  it('should call open() on the window object when openArticle() is called', () => {
    spyOn(window, 'open');
    component.openArticle('test');

    expect(window.open).toHaveBeenCalledWith('test', '_blank');
  });

  // Test source change here
  it('', () => {
    component['getArticlesByCount'](3, 'Top');
    fixture.detectChanges();
  });
});
