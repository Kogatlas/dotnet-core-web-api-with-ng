import {
  AddArticle,
  AddArticles,
  DeleteArticle,
  DeleteArticles,
  LoadArticles,
  UpdateArticle,
  UpdateArticles,
  UpsertArticle,
  UpsertArticles,
  ClearArticles,
} from './article.actions';
import { Article } from './article.model';
import { adapter, initialState, reducer } from './article.reducer';

describe('Article Reducer', () => {
  const article: Article = {
    id: 0,
    by: 'test',
    title: 'test',
    url: 'test'
  };

  describe('unknown action', () => {
    it('should return the initial state', () => {
      const action = {} as any;

      const result = reducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });

  it('should call addOne when an AddArticle action is triggered', () => {
    spyOn(adapter, 'addOne');
    reducer(initialState, new AddArticle({ article }));
    expect(adapter.addOne).toHaveBeenCalled();
  });

  it('should call upsertOne when an UpsertArticle action is triggered', () => {
    spyOn(adapter, 'upsertOne');
    reducer(initialState, new UpsertArticle({ article }));
    expect(adapter.upsertOne).toHaveBeenCalled();
  });

  it('should call addMany when an AddArticles action is triggered', () => {
    spyOn(adapter, 'addMany');
    reducer(initialState, new AddArticles({articles: [ article ]}));
    expect(adapter.addMany).toHaveBeenCalled();
  });

  it('should call upsertMany when an UpsertArticles action is triggered', () => {
    spyOn(adapter, 'upsertMany');
    reducer(initialState, new UpsertArticles({articles: [ article ]}));
    expect(adapter.upsertMany).toHaveBeenCalled();
  });

  it('should call updateOne when an UpdateArticle action is triggered', () => {
    spyOn(adapter, 'updateOne');
    reducer(initialState, new UpdateArticle({ article: { id: 0, changes: {} } }));
    expect(adapter.updateOne).toHaveBeenCalled();
  });

  it('should call updateMany when an UpdateArticles action is triggered', () => {
    spyOn(adapter, 'updateMany');
    reducer(initialState, new UpdateArticles({articles: [ { id: 0, changes: {} } ]}));
    expect(adapter.updateMany).toHaveBeenCalled();
  });

  it('should call removeOne when an DeleteArticle action is triggered', () => {
    spyOn(adapter, 'removeOne');
    reducer(initialState, new DeleteArticle({ id: article.id }));
    expect(adapter.removeOne).toHaveBeenCalled();
  });

  it('should call removeMany when an DeleteArticles action is triggered', () => {
    spyOn(adapter, 'removeMany');
    reducer(initialState, new DeleteArticles({ ids: [article.id] }));
    expect(adapter.removeMany).toHaveBeenCalled();
  });

  it('should call addAll when an LoadArticles action is triggered', () => {
    spyOn(adapter, 'addAll');
    reducer(initialState, new LoadArticles({articles: [ article ]}));
    expect(adapter.addAll).toHaveBeenCalled();
  });

  it('should call removeAll when an ClearArticles action is triggered', () => {
    spyOn(adapter, 'removeAll');
    reducer(initialState, new ClearArticles());
    expect(adapter.removeAll).toHaveBeenCalled();
  });
});
