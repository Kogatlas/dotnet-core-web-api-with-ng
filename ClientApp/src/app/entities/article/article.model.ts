export interface Article {
  // Attributes from the backend model
  by: string;
  descendants?: number;
  id: number;
  score?: number;
  time?: number;
  title: string;
  type?: string;
  url: string;
  // Attribute for driving the view
  sources?: string[];
}
